# EEE3097S PiHat Project 

The Raspberry Pi Remote Sensors HAT is a pHAT that allows you to have remote sensors that are connected through a Lora network, the HAT allows you to create a node on the network and be able to send data from connected sensors to a central location where you read the data and make decisions as you wish. The HAT has a LoRa module used to transmit the data between nodes to the LoRa gateway, it also has basic sensors that measure temperature, light, humidity and moisture there is an extension to add more sensors to the HAT. The HAT can be attached to a Raspberry Pi series board with 40 GPIO Pins. The HAT can be used in smart agriculture where a farmer needs to monitor key parameters of agriculture such as soil moisture, soil minerals, soil pH level, temperature etc. in order to improve crop productivity and resource use efficiency. 


### Bill of Material (BOM)

| BOM Level | Part Number | Part Name | Quantity | Unit Price (ZAR) | Link to material |
| ------ | ------ | ------ |------ |------ |------ |
| 02 | LBAA0QB1SJ-295 | LoRaWAN™ module | 01 | - | [link](https://www.murata.com/en-global/products/connectivitymodule/lpwa/overview/lineup/type-1sj) |
| 02 | SML-211UTT86 | 1.8 V Red LED | 01 | 7.06 | [link](https://za.rs-online.com/web/p/leds/7007907/?cm_mmc=ZA-PLA-DS3A-_-google-_-PLA_ZA_EN_Displays_%26_Optoelectronics_Whoop-_-(ZA:Whoop!)+LEDs-_-7007907&matchtype=&aud-821594433763:pla-340537785271&gclid=Cj0KCQjwyN-DBhCDARIsAFOELTmU3M7Bm5qqKmYXJymKUt8jRWCVbaU_SWx8rgxqWPqv9DyNwWQasgAaArEhEALw_wcB&gclsrc=aw.ds) |
| 02 | 1825910-6 | Single Pole Single Throw | 01 | 1.609 | [link](https://za.rs-online.com/web/p/tactile-switches/4791413/?cm_mmc=ZA-PLA-DS3A-_-google-_-PLA_ZA_EN_Switches_Whoop-_-(ZA:Whoop!)+Tactile+Switches-_-4791413&matchtype=&pla-531631261912&gclid=Cj0KCQjwyN-DBhCDARIsAFOELTkj0LRsEh64ya6gEhLdOV9BzV-hstCXKsMEebjG14HRESysT9C4NjwaAv02EALw_wcB&gclsrc=aw.ds) |
